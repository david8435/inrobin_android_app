
package com.inrobin.inrobin;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inrobin.inrobin.AlertsFragment.OnListFragmentInteractionListener;
import com.inrobin.inrobin.dummy.AlertContent;
import com.inrobin.inrobin.dummy.AlertContent.AlertItem;

import java.util.List;

import static java.security.AccessController.getContext;

/**
 * {@link RecyclerView.Adapter} that can display a {@link AlertContent.AlertItem} and makes a call to the
 * specified {@link AlertsFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyAlertsItemRecyclerViewAdapter extends RecyclerView.Adapter<MyAlertsItemRecyclerViewAdapter.ViewHolder> {

    private final List<AlertItem> mValues;
    private final AlertsFragment.OnListFragmentInteractionListener mListener;

    public MyAlertsItemRecyclerViewAdapter(List<AlertContent.AlertItem> items, AlertsFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_alertsitem, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mHeaderView.setText(mValues.get(position).header);
        holder.mContentView.setText(mValues.get(position).content);
        holder.mDateView.setText(mValues.get(position).date);

        //To alternate row colors in alerts listview
        if(position%2 == 0){
            holder.mItemLayout.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.lightRow));
        } else {
            holder.mItemLayout.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.darkRow));
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public LinearLayout mItemLayout;
        public final TextView mHeaderView;
        public final TextView mDateView;
        public final TextView mContentView;
        public AlertItem mItem;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mHeaderView = (TextView) view.findViewById(R.id.alert_header);
            mContentView = (TextView) view.findViewById(R.id.content);
            mDateView = (TextView) view.findViewById(R.id.alert_date);
            mItemLayout = (LinearLayout) view.findViewById(R.id.alert_item);
        }

        @Override
        public String toString() {
            return super.toString()+ " '" + mHeaderView.getText() + " '" + mContentView.getText() + "'" +mDateView.getText() +" '";
        }
    }
}

