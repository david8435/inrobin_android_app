package com.inrobin.inrobin;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Machine implements Parcelable {

    private String id;
    private String machine_name;
    private String machine_type;
    private String machine_component;
    private String performance;
    private String insurance;
    private String latitude;
    private String longitude;
    private Boolean machine_warning;

    public static Machine machineFromJson(JSONObject jsonObject) {
        Machine m = new Machine();
        // Deserialize json into object fields
        try {
            m.id = jsonObject.getString("id");
            m.machine_name = jsonObject.getString("machine_name");
            m.machine_type = jsonObject.getString("machine_type");
            m.machine_component = jsonObject.getString("machine_component");
            m.performance = jsonObject.getString("performance");
            m.insurance = jsonObject.getString("insurance");
            m.latitude = jsonObject.getString("latitude");
            m.longitude = jsonObject.getString("longitude");
            m.machine_warning = jsonObject.getBoolean("machine_warning");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        // Return new object
        return m;
    }

    public static ArrayList<Machine> machineFromJson(JSONArray jsonArray) {
        JSONObject machineJson;
        ArrayList<Machine> machines = new ArrayList<Machine>(jsonArray.length());
        // Process each result in json array, decode and convert to business object
        for (int i=0; i < jsonArray.length(); i++) {
            try {
                machineJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            Machine m = new Machine();
            m = Machine.machineFromJson(machineJson);
            if (m != null) {
                machines.add(m);
            }
        }

        return machines;
    }

    public String getId() {
        return id;
    }

    public String getMachine_name() {
        return machine_name;
    }

    public String getMachine_type() {
        return machine_type;
    }

    public String getMachine_component() {
        return machine_component;
    }

    public String getPerformance() {
        return performance;
    }

    public String getInsurance() {
        return insurance;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Boolean getMachine_warning() {
        return machine_warning;
    }

    @Override
    public String toString() {
        return "Machine{" +
                "id='" + id + '\'' +
                ", machine_name='" + machine_name + '\'' +
                ", machine_type='" + machine_type + '\'' +
                ", machine_component='" + machine_component + '\'' +
                ", performance='" + performance + '\'' +
                ", insurance='" + insurance + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", machine_warning=" + machine_warning +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

}
