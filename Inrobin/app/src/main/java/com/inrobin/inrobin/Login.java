package com.inrobin.inrobin;

import android.app.Activity;
import android.content.Intent;
import android.net.wifi.hotspot2.pps.Credential;
import android.net.wifi.hotspot2.pps.Credential.UserCredential;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class Login extends AppCompatActivity {

    private EditText input_email;
    private EditText input_password;
    private String myemail, mypassword;
    private Button button_login;
    private JSONObject myjson;
    private PostAsync myconn;
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private View.OnClickListener button_login_listener = new View.OnClickListener(){
        public void onClick(View v) {
            RunLogin();
        }
    };

    private void RunLogin(){
        myconn = new PostAsync();
        myemail = input_email.getText().toString();
        //input_email.setText("el mail: "+myemail);
        mypassword = input_password.getText().toString();
        //input_password.setText("el pass: "+mypassword);
        hideKeyboard(Login.this);
        try {
            myconn.execute("0","system/login", myemail, mypassword).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myjson = myconn.getResponse();
        if (myjson != null) {
            try {
                String token = myjson.getJSONObject("data").getString("token");
                String message = myjson.getString("message");
                Boolean valid = myjson.getBoolean("valid") ;
                if (valid) {
                    MySharedPreference.setUserCredentials(Login.this, myemail, mypassword);
                    //FirebaseApp.initializeApp(this);
                    NotificationUtils channels = new NotificationUtils(this);
                    Log.d("Login", "Starting Service, token: " + FirebaseInstanceId.getInstance().getInstanceId());
                    //startService(new Intent(this, NotificationService.class));

                    //Send the new firebase token to server if renewed
                    if (MySharedPreference.getRefreshToken()){
                        String firebaseToken = MySharedPreference.getFirebaseToken();
                        Log.d("Login", "firebase_token: "+firebaseToken);
                        NotificationService.sendRegistrationToServer(firebaseToken, token);
                        MySharedPreference.setRefreshToken(false);
                    }
                    goToMachineOverview(token);
                } else {
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getApplicationContext(), message, duration);
                    toast.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //new Timer().schedule(new Login.TimerListener(),2000);
        Log.d("Login", "despues del timer");
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    /*private class TimerListener extends TimerTask {
        @Override
        public void run() {
            myjson = myconn.getResponse();
            Log.d("Login", "durante timer " + myjson);
            if (myjson != null) {
                try {
                    String token = myjson.getJSONObject("data").getString("token");
                    goToMachineOverview(token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

    public void goToMachineOverview(String token){
        //Intent intent = new Intent(this, MachineOverview.class);
        Intent intent = new Intent(this, TabOverview.class);
        intent.putExtra("Token", token);
        Log.d("Login", "antes del cambiar de activity");
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FirebaseApp.initializeApp(this);
        // Example of a call to a native method
        input_email = (EditText) findViewById(R.id.input_email);
        input_password = (EditText) findViewById(R.id.input_password);
        button_login = (Button) findViewById(R.id.button_login);
        button_login.setOnClickListener(button_login_listener);
        if(MySharedPreference.getUserName(Login.this).length() != 0){
            input_email.setText(MySharedPreference.getUserName(Login.this));
            if(MySharedPreference.getUserPassword(Login.this).length() != 0){
                input_password.setText(MySharedPreference.getUserPassword(Login.this));
                RunLogin();
            }
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public void login(View view) {

    }
}