package com.inrobin.inrobin;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class JSONParser {

    String charset = "UTF-8";
    HttpsURLConnection conn;
    DataOutputStream wr;
    StringBuilder result;
    URL urlObj;
    JSONObject jObj = null;
    StringBuilder sbParams;
    String paramsString;

    public JSONObject makeHttpLoginRequest(String url, String method,
                                      HashMap<String, String> params, String token) {

        sbParams = new StringBuilder();
        int i = 0;

        //Uncomment this when trying in local
        HttpsURLConnection.setDefaultHostnameVerifier(WhitelistHostnameVerifier.INSTANCE);


        if (params != null){
            for (String key : params.keySet()) {
                try {
                    if (i != 0) {
                        sbParams.append("&");
                    }
                    sbParams.append(key).append("=")
                            .append(URLEncoder.encode(params.get(key), charset));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i++;
            }
        }

        if ((method.equals("POST")) && (url.equals(PostAsync.BASE_URL+"system/login"))) {
            // request method is POST
            try {
                urlObj = new URL(url);

                conn = (HttpsURLConnection) urlObj.openConnection();

                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                conn.setRequestProperty("Accept-Charset", charset);

                conn.setReadTimeout(10000);

                conn.setHostnameVerifier(WhitelistHostnameVerifier.INSTANCE);

                conn.setConnectTimeout(15000);

                paramsString = sbParams.toString();

                Log.e("JSON Parser", "Parsed params login: " + paramsString);

                wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(paramsString);
                wr.flush();
                wr.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if ((method.equals("POST")) && (url.equals(PostAsync.BASE_URL+"system/login")==false)) {
            // request method is POST
            try {
                urlObj = new URL(url);

                conn = (HttpsURLConnection) urlObj.openConnection();

                conn.setDoOutput(true);

                conn.setDoInput(true);

                conn.setRequestMethod("POST");

                conn.setRequestProperty("Content-Type", "application/" + "json;" + "charset=UTF-8");

                conn.setRequestProperty("authorization", "Bearer " + token);

                conn.setRequestProperty("Accept", "application/json");

                conn.setReadTimeout(10000);

                conn.setHostnameVerifier(WhitelistHostnameVerifier.INSTANCE);

                conn.setConnectTimeout(15000);

                //paramsString = sbParams.toString();
                JSONObject jsonparams = new JSONObject(params);
                //jsonparams.put("firebasetoken","asdfsfs");
                //jsonparams.put("token", token);

                Log.e("JSON Parser", "Parsed params, no login: " + jsonparams.toString());

                wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(jsonparams.toString());
                wr.flush();
                wr.close();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        if(method.equals("GET")){
            // request method is GET

            /*if (sbParams.length() != 0) {
                url += "?" + sbParams.toString();
            }*/

            try {
                urlObj = new URL(url);

                conn = (HttpsURLConnection) urlObj.openConnection();

                conn.setDoOutput(false);

                conn.setRequestMethod("GET");

                conn.setRequestProperty("Content-Type", "application/" + "json");

                conn.setRequestProperty("authorization", "Bearer " + token);

                conn.setHostnameVerifier(WhitelistHostnameVerifier.INSTANCE);

                conn.setConnectTimeout(15000);

                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            //Receive the response from the server
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            Log.d("JSON Parser", "result: " + result.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.disconnect();

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(result.toString());
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON Object
        return jObj;
    }

    public static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }
        } };

        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    enum WhitelistHostnameVerifier implements HostnameVerifier {
        // these hosts get whitelisted
        INSTANCE("localhost", "sub.domain.com");

        private Set whitelist = new HashSet<>();
        private HostnameVerifier defaultHostnameVerifier =
                HttpsURLConnection.getDefaultHostnameVerifier();

        WhitelistHostnameVerifier(String... hostnames) {
            for (String hostname : hostnames) {
                whitelist.add(hostname);
            }
        }

        @Override
        public boolean verify(String host, SSLSession session) {
            if (whitelist.contains(host)) {
                return true;
            }
            // important: use default verifier for all other hosts
            return defaultHostnameVerifier.verify(host, session);
        }
    }
}
