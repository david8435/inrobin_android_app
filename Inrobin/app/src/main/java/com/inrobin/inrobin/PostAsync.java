package com.inrobin.inrobin;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


class PostAsync extends AsyncTask<String, String, JSONObject> {
    JSONParser jsonParser = new JSONParser();

    private ProgressDialog pDialog;
    private static JSONObject response;

    public static final String BASE_URL = "https://server.inrobin.com/";

    //localhost Ip from emulator
    //private static final String BASE_URL = "https://10.0.2.2:3000/";

    private static final String TAG_SUCCESS = "valid";
    private static final String TAG_MESSAGE = "data";



    /*@Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Attempting login...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }*/

    private void setResponse(JSONObject res){
        //Log.d("PostAsync reponse", this.response.toString());
        response = res;
    }

    public JSONObject getResponse(){
        return response;
    }

    /*
    arg[0] -> tipo post: 0 = login, 1 = machineOverveiw
    arg[1] -> url end (the part needed after the BASE_URL
     */
    @Override
    protected JSONObject doInBackground(String... args) {

        try {
            JSONObject json;
            HashMap<String, String> params = new HashMap<>();
            Integer operation = Integer.parseInt(args[0]);
            switch (operation) {
                case 0:
                    params = new HashMap<>();
                    params.put("username", args[2]);
                    params.put("password", args[3]);
                    Log.d("PostAsync operation ", args[0]);
                    json = jsonParser.makeHttpLoginRequest(
                            BASE_URL+args[1], "POST", params, null);
                    break;
                case 1:
                    Log.d("PostAsync operation ", args[0]);
                    Log.d("PostAsync token ", args[2]);
                    json = jsonParser.makeHttpLoginRequest(
                            BASE_URL+args[1], "GET", null, args[2]);

                    break;

                case 2:
                    Log.d("PostAsync operation ", args[0]);
                    Log.d("PostAsync token ", args[2]);
                    params = new HashMap<>();
                    params.put("firebasetoken", args[3]);
                    json = jsonParser.makeHttpLoginRequest(
                            BASE_URL+args[1], "POST", params, args[2]);

                default:
                    params = new HashMap<>();
                    params.put("username", args[0]);
                    params.put("password", args[1]);
                    json = jsonParser.makeHttpLoginRequest(
                            BASE_URL+args[1], "POST", params, null);
                    break;
            }



            Log.d("PostAsync request", "starting");


            if (json != null) {

                Log.d("PostAsync json", json.toString());
                this.setResponse(json);

                return json;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject json) {

        boolean success = false;
        String message = "";

        /*if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }*/
        if (json != null) {
            /*Toast.makeText(login.this, json.toString(),
                    Toast.LENGTH_LONG).show();*/

            try {
                success = json.getBoolean(TAG_SUCCESS);
                message = json.getString(TAG_MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("PostAsync setResponse", json.toString());
        }

        if (success == true) {
            Log.d("PostAsync Success!", message);
        }else{
            Log.d("PostAsync Failure", message);
        }
    }

}