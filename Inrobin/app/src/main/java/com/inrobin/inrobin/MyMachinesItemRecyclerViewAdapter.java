package com.inrobin.inrobin;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inrobin.inrobin.MachinesFragment.OnListFragmentInteractionListener;
import com.inrobin.inrobin.dummy.MachineContent.MachineItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MachineItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyMachinesItemRecyclerViewAdapter extends RecyclerView.Adapter<MyMachinesItemRecyclerViewAdapter.ViewHolder> {

    private final List<MachineItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyMachinesItemRecyclerViewAdapter(List<MachineItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_machinesitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        //holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);
        holder.machineBadge.setText("!");

        //This is to show alerts
        if (mValues.get(position).machine_warning == true){
            holder.machineBadge.setVisibility(View.VISIBLE);
        }
        else{
            holder.machineBadge.setVisibility(View.GONE);
        }

        Log.d("MachinesRecycler", "Performance value: "+mValues.get(position).performance);
        Log.d("MachinesRecycler", "Maintenance value: "+mValues.get(position).maintenance);
        //Performance lights
        if(mValues.get(position).performance.equals("Disconnected")){
            holder.performanceBadgeGray.setVisibility(View.VISIBLE);
            holder.performanceBadgeGreen.setVisibility(View.GONE);
            holder.performanceBadgeRed.setVisibility(View.GONE);
            holder.performanceBadgeYellow.setVisibility(View.GONE);
        }
        if(mValues.get(position).performance.equals("Downtime")){
            holder.performanceBadgeGray.setVisibility(View.INVISIBLE);
            holder.performanceBadgeGreen.setVisibility(View.GONE);
            holder.performanceBadgeRed.setVisibility(View.VISIBLE);
            holder.performanceBadgeYellow.setVisibility(View.GONE);
        }
        if(mValues.get(position).performance.equals("Critical")){
            holder.performanceBadgeGray.setVisibility(View.INVISIBLE);
            holder.performanceBadgeGreen.setVisibility(View.GONE);
            holder.performanceBadgeRed.setVisibility(View.GONE);
            holder.performanceBadgeYellow.setVisibility(View.VISIBLE);
        }
        if(mValues.get(position).performance.equals("Working")){
            holder.performanceBadgeGray.setVisibility(View.INVISIBLE);
            holder.performanceBadgeGreen.setVisibility(View.VISIBLE);
            holder.performanceBadgeRed.setVisibility(View.GONE);
            holder.performanceBadgeYellow.setVisibility(View.GONE);
        }

        //Maintenance lights
        if(mValues.get(position).maintenance.equals("Renewal")){
            holder.maintenanceBadgeBlue.setVisibility(View.VISIBLE);
            holder.maintenanceBadgeGreen.setVisibility(View.GONE);
            holder.maintenanceBadgeRed.setVisibility(View.GONE);
            holder.maintenanceBadgeYellow.setVisibility(View.GONE);
        }
        if(mValues.get(position).maintenance.equals("NotInsured")){
            holder.maintenanceBadgeBlue.setVisibility(View.INVISIBLE);
            holder.maintenanceBadgeGreen.setVisibility(View.GONE);
            holder.maintenanceBadgeRed.setVisibility(View.VISIBLE);
            holder.maintenanceBadgeYellow.setVisibility(View.GONE);
        }
        if(mValues.get(position).maintenance.equals("Less1Month")){
            holder.maintenanceBadgeBlue.setVisibility(View.INVISIBLE);
            holder.maintenanceBadgeGreen.setVisibility(View.GONE);
            holder.maintenanceBadgeRed.setVisibility(View.GONE);
            holder.maintenanceBadgeYellow.setVisibility(View.VISIBLE);
        }
        if(mValues.get(position).maintenance.equals("More1Month")){
            holder.maintenanceBadgeBlue.setVisibility(View.INVISIBLE);
            holder.maintenanceBadgeGreen.setVisibility(View.VISIBLE);
            holder.maintenanceBadgeRed.setVisibility(View.GONE);
            holder.maintenanceBadgeYellow.setVisibility(View.GONE);
        }


        //This is for alternate row colors
        if(position%2 == 0){
            holder.mItemLayout.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.lightRow));
        } else {
            holder.mItemLayout.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.darkRow));
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public RelativeLayout mItemLayout;
        //public final TextView mIdView;
        public final TextView mContentView, machineBadge;
        //Performance and Maintenance lights
        public final TextView  performanceBadgeGray, performanceBadgeGreen, performanceBadgeRed, performanceBadgeYellow;
        public final TextView  maintenanceBadgeBlue, maintenanceBadgeGreen, maintenanceBadgeRed, maintenanceBadgeYellow;
        public MachineItem mItem;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mItemLayout = (RelativeLayout) view.findViewById(R.id.machine_item);
            //mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            machineBadge = (TextView) view.findViewById(R.id.machineAlertBadge);

            performanceBadgeGray = (TextView) view.findViewById(R.id.performanceBadgeGray);
            performanceBadgeGreen = (TextView) view.findViewById(R.id.performanceBadgeGreen);
            performanceBadgeYellow = (TextView) view.findViewById(R.id.performanceBadgeYellow);
            performanceBadgeRed = (TextView) view.findViewById(R.id.performanceBadgeRed);

            maintenanceBadgeBlue = (TextView) view.findViewById(R.id.maintenanceBadgeBlue);
            maintenanceBadgeGreen = (TextView) view.findViewById(R.id.maintenanceBadgeGreen);
            maintenanceBadgeYellow = (TextView) view.findViewById(R.id.maintenanceBadgeYellow);
            maintenanceBadgeRed = (TextView) view.findViewById(R.id.maintenanceBadgeRed);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
