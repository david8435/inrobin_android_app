package com.inrobin.inrobin;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.inrobin.inrobin.dummy.AlertContent;
import com.inrobin.inrobin.dummy.MachineContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class TabOverview extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private PostAsync myconnMachines, myconnAlerts, myconnUnreadMessages;
    private JSONObject machineList, alertList, unreadMessagesJson;
    private MachineContent machineItems;
    private AlertContent alertItems;
    private static String token;
    private TabLayout tabLayout;
    private String[] tabTitle={"Machines","Alerts"};
    private int[] unreadCount={0,0};

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        token = intent.getStringExtra("Token");
        setContentView(R.layout.activity_tab_overview);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.getMachineListData();
        this.getAlertsListData();
        this.getUnreadMessages();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        tabLayout.setupWithViewPager(mViewPager);

        try
        {
            setupTabIcons();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setupTabIcons() {

        for(int i=0;i<tabTitle.length;i++)
        {
            /*TabLayout.Tab tabitem = tabLayout.newTab();
            tabitem.setCustomView(prepareTabView(i));
            tabLayout.addTab(tabitem);*/
            Log.d("TAB OVERVIEW", "setcustomview: "+ tabLayout.getTabAt(i));
            tabLayout.getTabAt(i).setCustomView(prepareTabView(i));
        }
    }

    private View prepareTabView(int pos) {
        View view = getLayoutInflater().inflate(R.layout.custom_tab, null);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_count = (TextView) view.findViewById(R.id.tv_count);
        LinearLayout badge_layout = (LinearLayout) view.findViewById(R.id.badgeCotainer);
        tv_title.setText(tabTitle[pos]);;
        if(unreadCount[pos]>0)
        {
            Log.d("TAB OVERVIEW", "tv_count_visible ");
            badge_layout.setVisibility(View.VISIBLE);
            tv_count.setText(""+unreadCount[pos]);
        }
        else
            badge_layout.setVisibility(View.GONE);

        return view;
    }

    private void getMachineListData(){
        // Getting the machine data

        myconnMachines = new PostAsync();
        try {
            myconnMachines.execute("1","machine/list", token).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        machineList = myconnMachines.getResponse();
        Log.d("TAB OVERVIEW", "MACHINE LIST "+ machineList);
        JSONArray machinesJson = null;
        try {
            machinesJson = machineList.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Filling machines listview
        //final ListView lvmachines = (ListView) findViewById(R.id.listMachines);


        machineItems = new MachineContent();
        //This clear previous items in listview
        machineItems.ITEMS.clear();
        String machine_name = null;
        String machine_id = null;
        Boolean machine_warning = false;
        String machine_performance = "Downtime";
        String machine_maintenance = "Downtime";
        for(int i=0; i < machinesJson.length() ; i++) {
            try {
                machine_name = machinesJson.getJSONObject(i).getString("machine_name");
                machine_id=machinesJson.getJSONObject(i).getString("id");
                machine_warning=machinesJson.getJSONObject(i).getBoolean("machine_warning");
                machine_performance=machinesJson.getJSONObject(i).getString("performance");
                machine_maintenance=machinesJson.getJSONObject(i).getString("insurance");
            } catch (JSONException e) {
                e.printStackTrace();
            };
            //machineItems.add(machine_id);
            machineItems.addItem(new MachineContent.MachineItem(machine_id,machine_name,
                    "details", machine_warning, machine_performance, machine_maintenance));
            Log.d(machine_name,"Output "+i);
        }

    }

    private void getAlertsListData(){
        //Getting alerts data

        myconnAlerts = new PostAsync();
        try {
            myconnAlerts.execute("1","messages?page=/overview/", token).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        alertList = myconnAlerts.getResponse();
        Log.d("ALERTS", "ALERTS LIST "+ alertList);
        JSONArray alertsJson = null;
        try {
            alertsJson = alertList.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        alertItems = new AlertContent();
        alertItems.ITEMS.clear();
        String message_id = null;
        String message_content = null;
        String message_header = null;
        String message_create_time = null;
        String message_update_time = null;
        for(int i=0; i < alertsJson.length() ; i++) {
            try {
                message_id = alertsJson.getJSONObject(i).getString("id");
                message_header = alertsJson.getJSONObject(i).getString("header");
                message_content = alertsJson.getJSONObject(i).getString("message_text");
                message_create_time = alertsJson.getJSONObject(i).getString("created_at");
                message_update_time = alertsJson.getJSONObject(i).getString("updated_at");
            } catch (JSONException e) {
                e.printStackTrace();
            };
            alertItems.addItem(new AlertContent.AlertItem(message_id, message_header, message_content, message_update_time));
        }
    }

    private void getUnreadMessages(){
        //Getting UnreadMessages
        myconnUnreadMessages = new PostAsync();
        try {
            myconnUnreadMessages.execute("1","/message/unread_count", token).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        unreadMessagesJson = myconnUnreadMessages.getResponse();
        Log.d("TabOverview", "UNREAD MESSAGES "+ unreadMessagesJson);
        Integer unreadMessages = 0;
        try {
            unreadMessages = unreadMessagesJson.getInt("data");
            unreadCount[1] = unreadMessages;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    MachinesFragment machinesFrag = new MachinesFragment();
                    Bundle bundle = new Bundle();
                    machinesFrag.setArguments(bundle);
                    Log.d("TAB OVERVIEW ", "machineItems: "+machineItems.toString());
                    //bundle.putParcelableArrayList("machines", machineItems);
                    machinesFrag.setArguments(bundle);
                    return machinesFrag;
                /*case 2:
                    MachinesFragment machinesFrag2 = new MachinesFragment();
                    Bundle bundle2 = new Bundle();
                    machinesFrag2.setArguments(bundle2);
                    Log.d("TAB OVERVIEW ", "machineItems: "+machineItems.toString());
                    //bundle2.putStringArrayList("machines", machineItems);
                    machinesFrag2.setArguments(bundle2);
                    return machinesFrag2;*/
                case 1:
                    AlertsFragment alertsFrag = new AlertsFragment();
                    Bundle bundle3 = new Bundle();
                    alertsFrag.setArguments(bundle3);
                    Log.d("TAB OVERVIEW ", "AlertItems: "+machineItems.toString());
                    //bundle3.putStringArrayList("machines", machineItems);
                    alertsFrag.setArguments(bundle3);
                    return alertsFrag;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }
    }

}
