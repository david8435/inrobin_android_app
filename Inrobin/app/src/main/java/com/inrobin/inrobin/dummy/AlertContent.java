package com.inrobin.inrobin.dummy;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AlertContent {

    /**
     * An array of sample (Alert) items.
     */
    public static final List<AlertContent.AlertItem> ITEMS = new ArrayList<AlertContent.AlertItem>();

    /**
     * A map of sample (Alert) items, by ID.
     */
    public static final Map<String, AlertContent.AlertItem> ITEM_MAP = new HashMap<String, AlertContent.AlertItem>();



    public static void addItem(AlertContent.AlertItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static AlertContent.AlertItem createAlertItem(int position) {
        return null;
        //return new AlertContent.AlertItem(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A Machine item representing a piece of content.
     */
    public static class AlertItem {
        public final String id;
        public final String header;
        public final String content;
        public String date;

        public AlertItem(String id, String header, String content, String date) {
            this.id = id;
            this.header = header;
            this.content = content;
            this.date = "";
            //SimpleDateFormat inputdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat outputdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            try {
                Date d = outputdate.parse(date);
                this.date = d.toString();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            //String formattedTime = outputdate.format(d);
            //this.date = outputdate.toString();
        }

        @Override
        public String toString() {
            return header + " " + content + " " + date;
        }
    }
}
