package com.inrobin.inrobin;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

//Not secure solution, just provisional

public class MySharedPreference {
    static String PREF_USER_NAME= "username";
    static String PREF_USER_PASS= "password";
    static Boolean REFRESH_TOKEN= false;
    static String FIREBASE_TOKEN= "myToken";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserCredentials(Context ctx, String userName, String userPassword)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.putString(PREF_USER_PASS, encrypt(userPassword));
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static String getUserPassword(Context ctx)
    {
        return decrypt(getSharedPreferences(ctx).getString(PREF_USER_PASS, ""));
    }

    public static String encrypt(String input) {
        // This is base64 encoding, which is not an encryption
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    public static String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

    public static Boolean getRefreshToken() {
        return REFRESH_TOKEN;
    }

    public static void setRefreshToken(Boolean refreshToken) {
        REFRESH_TOKEN = refreshToken;
    }

    public static String getFirebaseToken() {
        return FIREBASE_TOKEN;
    }

    public static void setFirebaseToken(String firebaseToken) {
        FIREBASE_TOKEN = firebaseToken;
    }
}
