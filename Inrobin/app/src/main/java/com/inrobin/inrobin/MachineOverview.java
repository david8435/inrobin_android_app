package com.inrobin.inrobin;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.app.ListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


//OLD CLASS, NOW APP USES TabOverview

public class MachineOverview extends AppCompatActivity {

    private static String token;
    private PostAsync myconn, myconnalerts;
    private JSONObject myjson;
    private ArrayList<Machine> machineArray;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.d("Machine Overview ", "running menu");
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            //Back button
            case android.R.id.home:
                //If this activity started from other activity
                finish();

            /*If you wish to open new activity and close this one
            startNewActivity();
            */
                return true;
            case R.id.exit:
                goBackToLogin();
                return true;
            case R.id.alerts:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goBackToLogin(){
        Intent intent = new Intent(this, Login.class);
        Log.d("Login", "antes del cambiar de activity");
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_overview);
        Intent intent = getIntent();
        token = intent.getStringExtra("Token");
        Log.d("Token in overview ", token);

        // Getting the machine data

        myconn = new PostAsync();
        try {
            myconn.execute("1","machine/list", token).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myjson = myconn.getResponse();
        Log.d("OVERVIEW", "MACHINE LIST "+ myjson);
        JSONArray machinesJson = null;
        try {
            machinesJson = myjson.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Filling machines listview
        final ListView lvmachines = (ListView) findViewById(R.id.listMachinesOverview);

        ArrayList<String> machineItems = new ArrayList<String>();
        String machine_name = null;
        String machine_id = null;
        for(int i=0; i < machinesJson.length() ; i++) {
            try {
                machine_name = machinesJson.getJSONObject(i).getString("machine_name");
                machine_id=machinesJson.getJSONObject(i).getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            };
            machineItems.add(machine_name);
            Log.d(machine_name,"Output "+i);
        }
        ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(this,
                R.layout.fragment_machinesitem, R.id.content, machineItems);
        lvmachines.setAdapter(mArrayAdapter);
        Log.d("despues del adapter", machineItems.toString());

/*
        // Getting the alerts data
        final ListView lvmessages = (ListView) findViewById(R.id.listMessages);

        myconnalerts = new PostAsync();
        try {
            myconnalerts.execute("1","messages?page=/overview/", token).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myjson = myconnalerts.getResponse();
        Log.d("ALERTS", "ALERTS LIST "+ myjson);
        JSONArray messagesJson = null;
        try {
            messagesJson = myjson.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<String> messageItems = new ArrayList<String>();
        String message_content = null;
        String message_header = null;
        for(int i=0; i < messagesJson.length() ; i++) {
            try {
                message_header = messagesJson.getJSONObject(i).getString("header");
                message_content = messagesJson.getJSONObject(i).getString("message_text");
            } catch (JSONException e) {
                e.printStackTrace();
            };
            messageItems.add(message_content);
        }
        ArrayAdapter<String> messArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_expandable_list_item_1, messageItems);
        lvmessages.setAdapter(messArrayAdapter);
        */
    }


   /* private class TimerListener extends TimerTask {
        @Override
        public void run() {
            myjson = myconn.getResponse();
            Log.d("OVERVIEW", "MACHINE LIST "+ myjson);

            if (myjson != null){
                try {
                    if (myjson.getBoolean("valid")){
                        JSONArray machinesJson = myjson.getJSONArray("data");
                        Log.d("OVERVIEW", "MACHINE LIST "+ machinesJson);
                        machineArray = Machine.machineFromJson(machinesJson);
                    }
                    else{
                        machineArray = null;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/
}
