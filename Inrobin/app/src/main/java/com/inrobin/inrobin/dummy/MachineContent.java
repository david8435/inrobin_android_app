package com.inrobin.inrobin.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MachineContent {

    /**
     * An array of sample (Machine) items.
     */
    public static final List<MachineContent.MachineItem> ITEMS = new ArrayList<MachineContent.MachineItem>();

    /**
     * A map of sample (Machine) items, by ID.
     */
    public static final Map<String, MachineContent.MachineItem> ITEM_MAP = new HashMap<String, MachineContent.MachineItem>();



    public static void addItem(MachineContent.MachineItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static MachineContent.MachineItem createMachineItem(int position) {
        return new MachineContent.MachineItem(String.valueOf(position), "Item " + position, makeDetails(position),
                false, "Disconnected" , "Disconnected");
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A Machine item representing a piece of content.
     */
    public static class MachineItem {
        public final String id;
        public final String content;
        public final String details;
        public final boolean machine_warning;
        public final String performance;
        public final String maintenance;

        public MachineItem(String id, String content, String details, Boolean machine_warning,
                           String performance, String maintenance) {
            this.id = id;
            this.content = content;
            this.details = details;
            this.machine_warning = machine_warning;
            this.performance = performance;
            this.maintenance = maintenance;
        }

        @Override
        public String toString() {
            return this.content;
        }
    }
}
